package com.leetcode.strategy.Arrays.OneRotateArrayInJava;

public class Solution_3_Reversal {
public static void arrayRotationUsingReversalMethod(int input[],int rotateNumber)
{
    int a = input.length - rotateNumber;
//       input array ; 1,2,3,4,5,6
//    1. Divide the array two parts: 1,2,3,4 and 5, 6
//    2. Rotate first part: 4,3,2,1,5,6
//    3. Rotate second part: 4,3,2,1,6,5
//    4. Rotate the whole array: 5,6,1,2,3,4
    reverse(input,0, a-1);
    reverse(input,a, input.length-1);
    reverse(input,0, input.length-1);

}
    public static void reverse(int input[],int left,int right)
    {
        while(left<right)
        {
            int temp=input[left];
            input[left]=input[right];
            input[right]=temp;
        left++;
        right--;
        }
    }




public static void main(String args[])
{
    int inputArray[]={1,2,3,4,5,6};
    Solution_3_Reversal.arrayRotationUsingReversalMethod(inputArray,3);
    System.out.println("Final Output ");
    for(int i:inputArray)
    {
        System.out.println(i);
    }
}
}
