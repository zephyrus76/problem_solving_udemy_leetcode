package com.leetcode.strategy.Arrays.OneRotateArrayInJava;

public class Solution_2_Bubble_Rotate {
    public static void arrayRotationUsingBubbleRotate(int inputArray[],int rotateNumber)
    {
      for (int i=0;i<rotateNumber;i++)
      {
      for (int j= inputArray.length-1;j>0;j--)
      {
      int temp=inputArray[j];
      inputArray[j]=inputArray[j-1];
      inputArray[j-1]=temp;
      }
      }

    }


    public static void main(String args[])
    {
        /////////////////////Solving the broblem via bubble rotate //////////
        int inputArray[]={1,2,3,4,5,6,7};
        Solution_2_Bubble_Rotate.arrayRotationUsingBubbleRotate(inputArray,3);
        System.out.println("Final Output ");
        for(int i:inputArray)
        {
            System.out.println(i);
        }
    }
/////////////hi
}
