package com.leetcode.strategy.Arrays.OneRotateArrayInJava;
public class Solution_1_IntermediateArray
        {
public static void arrayRotationUsingIntermediateArray(int inputArray[],int rotationNumber)  //rotationNumber is k
{
/////////////// Copying the element in the intermediate Array ///////////////////////
        int intermediateArray[] = new int[inputArray.length];
        int j = 0;
///////////// copying first k to n-1 ////////////////////////
        for (int i = rotationNumber+1; i < inputArray.length; i++) {
                intermediateArray[j] = inputArray[i];
                j++;
        }
///////////// copying 0 to k //////////////////////
        for (int i = 0; i <= rotationNumber; i++) {
                intermediateArray[j] = inputArray[i];
                j++;
        }

////// copying the intermediate array result back to original input array /////////////////
        System.arraycopy(intermediateArray, 0, inputArray, 0, inputArray.length);
}


public static void main(String args[])
{
        int inputArray[]={1,2,3,4,5,6,7};
        Solution_1_IntermediateArray.arrayRotationUsingIntermediateArray(inputArray,3);
        System.out.println("Final Output ");
        for(int i:inputArray)
        {
                System.out.println(i);
        }
}





        }
