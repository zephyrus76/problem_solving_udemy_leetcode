package com.leetcode.strategy.Stack.EvaluateReversePolishNotation;

import java.util.Stack;

public class SolutionUsingStacks {

public  static String evaluatePolishNotation(String inputArray[])
{
    //Using Stack for storing number and intermediate results
    Stack<String> stack=new Stack<>();

    String operators = "+-*/";

    for(String input:inputArray)
    {
        if(!operators.contains(input))
        {
          stack.push(input);
        }
        else
        {
          int b=Integer.valueOf(stack.pop());
          int a=Integer.valueOf(stack.pop());

          switch (input)
          {
              case "+": stack.push(String.valueOf(a+b)); break;
              case "-": stack.push(String.valueOf(a-b)); break;
              case "/": stack.push(String.valueOf(a/b)); break;
              case "*": stack.push(String.valueOf(a*b)); break;
          }

        }
    }
    return stack.pop();
}



public static void main(String args[])
{
 ////taking the String array as an input containing numbers and operators //////

 String inputArray[]={"2","1","+","3","*"};
    System.out.println("The Result is "+evaluatePolishNotation(inputArray));
}


}
