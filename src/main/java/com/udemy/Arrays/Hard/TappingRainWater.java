package com.udemy.Arrays.Hard;

public class TappingRainWater {

    public static int optimizedSolution(int input[])
    {


        int area=0;
        int leftHeight=0;
        int rightHeight= 0;

        //////////////for calculating right height /////////////////
        for(int i=input.length-1;i>=0;i--)
        {
            if(input[i] > 0)
            {
                rightHeight=i;
           break;
            }
        }


        int currentHeight=leftHeight+1;
        while(currentHeight!=rightHeight)
        {
            if(input[currentHeight] > input[leftHeight])
            {
                leftHeight=currentHeight;
            }
            else
            {
                area=area+(Math.min(input[leftHeight],input[rightHeight])-input[currentHeight]);

            }
            currentHeight++;
        }


        return area;
    }



public static void main(String args[])
{
    int inputArray[]={0,1,0,2,1,0,3,1,0,1,2};

    System.out.println("The area is "+ optimizedSolution(inputArray));
}
}
