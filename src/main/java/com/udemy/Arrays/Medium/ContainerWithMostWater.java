package com.udemy.Arrays.Medium;

public class ContainerWithMostWater {

    public static long bruteForceSolution(int input[]) {
        //In this approach we will analyze all the pairs in the array and stored in a variable with the highest area
        long area = 0;

        for (int i = 0; i < input.length; i++) {
            for (int j = i + 1; j < input.length; j++) {
                long interArea = Math.min(input[i], input[j]) * (j - i);
                if (interArea > area) {
                    area = interArea;
                }
            }
        }
        return area;
    }

    public static int optimizedSolution(int input[])
    {
    /* In this approach we will use left and right pointer approach where we will calculate the area and move towards
      the center but we will move only those pointer which has lesser height only */
        int area=0;
        int leftHeight=0;
        int rightHeight=input.length-1;

        while(leftHeight!=rightHeight)
        {
            int interArea=Math.min(input[leftHeight],input[rightHeight])*(rightHeight-leftHeight);
            if (interArea > area) {
                area = interArea;
            }
            if (input[leftHeight]<input[rightHeight])
            {
                leftHeight++;
            }
            else
            {
                rightHeight--;
            }
        }
        return area;
    }

    public static void main(String args[]) {
        int input[] = {1, 8, 6, 2, 9, 4};
        System.out.println("The max area by brute force  is " + bruteForceSolution(input));
        System.out.println("The max area by optimized solution is " + optimizedSolution(input));

    }


}
