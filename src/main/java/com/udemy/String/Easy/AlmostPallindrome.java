package com.udemy.String.Easy;

public class AlmostPallindrome {
public static boolean isPallindrome(String input)
{
    int left=0,right=input.length()-1;
    while(left<right)
    {
        if (input.charAt(left)==input.charAt(right))
        {
            left++;
            right--;
        }
        else
        {
            return false;
        }
    }
    return true;
}

public static boolean isAlmostPallindrome(String input)
{
    //left and right are two pointer to traversing the given string from both sides
    int left=0,right=input.length()-1;
    while(left<right)
    {
        if (input.charAt(left)==input.charAt(right))
        {
            left++;
            right--;
        }
        else
        {
            String input1=input.substring(0,left)+input.substring(left+1);
            String input2=input.substring(0,right)+input.substring(right+1);

            return (isPallindrome(input1) || isPallindrome(input2));
        }
    }

    return true;
}

    public static void main(String args[])
{
String input="abbacasda";
    System.out.println("output is "+isAlmostPallindrome(input));

    System.out.println("the substr "+input.substring(0,5)
             +
            input.substring(5+1)
            );
}

}
