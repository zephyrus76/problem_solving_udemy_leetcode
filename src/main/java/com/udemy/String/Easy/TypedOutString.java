package com.udemy.String.Easy;
import java.util.*;
public class TypedOutString {

    public static String afterTypedOutBruteForce(String input)
    {
         char output[]=new char[input.length()];
         int i=0;
         int backspaceCount=0;
         for(int j=input.length()-1;j>0;j--)
         {
             if(input.charAt(j) == '#')
             {
                 backspaceCount=backspaceCount+1;
             }
             else if(backspaceCount > 0 && input.charAt(j)!='#')
             {
                 backspaceCount=backspaceCount-1;
             }
             else
             {
                output[i]= input.charAt(j);
                i++;
             }
         }
       // System.out.println("The output string is as follow for "+input+" is "+new String(output).trim());
         return new String(output).trim();
    }






    public static void main(String args[])
{
/////For Given two String find out they are equal after being typed out ////
String input1="a##b";
String input2="c##b#b";

    System.out.println("The given two string is Typed out ? "+afterTypedOutBruteForce(input1).equals(afterTypedOutBruteForce(input2)));


}


}
