package com.udemy.String.Medium;

import java.util.HashMap;
import java.util.HashSet;

public class LengthOfLogestSustringWithoutRpeatingCharacter {

    public static int bruteForceSoution(String input)
    {
        //intializing the largent substr length
        int largestLength=0;

        for (int left=0;left<input.length();left++)
        {
            HashSet<String> ch=new HashSet<String>();
            int tempLength=0;
        for (int right=left;right<input.length();right++)
        {
            if(!ch.contains(input.charAt(right)+"") )
            {
                ch.add(input.charAt(right)+"");
                tempLength++;
                largestLength=Math.max(largestLength,tempLength);
            }
            else
            {
                break;
            }
        }

        }


        return largestLength;
    }

    public static int optimizedSolution(String input)
    {
        HashMap<String,Integer> hmap=new HashMap<>();

        int left=0;
        int length=0;
        for (int right=0;right<input.length();right++)
        {
           String currentCharacter=input.charAt(right)+"";
          if(!hmap.containsKey(currentCharacter))
          {
              hmap.put(currentCharacter,right);
          }
          else
          {
            //  length=Math.max(length,right-left);
            if (left < hmap.get(currentCharacter)) {
                left = hmap.get(currentCharacter) + 1;
                hmap.put(currentCharacter, right);
            }
            }
            length=Math.max(length,right-left+1);
        }
        return length;
    }


    public static void main(String args[])
    {
        String input="abba";
        System.out.println("The max length by BruteForce Solution is "+bruteForceSoution(input));
        System.out.println("The max length by Optimized Solution is "+optimizedSolution(input));


    }
}
