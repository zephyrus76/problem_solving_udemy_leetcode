package com.udemy.SingleLinkedList;

public class LinkedList {

    private Object value;
    private LinkedList next;


    @Override
    public String toString() {
        return "LinkedList{" +
                "value=" + value +
              //  ", next=" + next +
                '}';
    }

    public LinkedList(Object value, LinkedList next) {
        this.value = value;
        this.next = next;
    }

    public LinkedList getNext() {
        return next;
    }

    public void setNext(LinkedList next) {
        this.next = next;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
