package com.udemy.SingleLinkedList;

public class ReverseTheLinkedList {

    public static void main(String args[])
    {

        LinkedList l4=new LinkedList(12,null);
        LinkedList l3=new LinkedList(13,l4);
        LinkedList l2=new LinkedList(14,l3);
        LinkedList l1=new LinkedList(15,l2);

       LinkedList currentHead=l1;
       LinkedList prev=null;

       while(currentHead!=null)
       {
           LinkedList next=currentHead.getNext();
           currentHead.setNext(prev);
           prev=currentHead;
           currentHead=next;
       }

       ////// 1->2->3->null
       /*
         currentHead=1
         prev=null;

         while(1->2->3->null!=null)
         {
         next=2->3->null;
         1.next=null;      null<-1
         prev= null <- 1
         currentHead= 2->3->null
         }

        while(2->3->null!=null)
         {
         next=3->null;
         2.next= null<-1;      null<-1<-2
         prev= null<-1<-2
         currentHead= 3->null
         }
         while(3->null)
         {
         next=null;
         3.next= null<-1<-2;      null<-1<-2<-3
         prev= null<-1<-2<-3
         currentHead= null
         }
       */
        LinkedList c=prev;
        while(c!=null)
        {
            System.out.println(c.getValue());
            c=c.getNext();
        }
    }



}
